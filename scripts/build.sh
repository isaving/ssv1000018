#!/bin/bash
set -e -x

cd $(dirname $0)/..
source $(dirname $0)/version.sh

go fmt ./...

rm -rf bin && mkdir bin
LINKFLAGS="-linkmode external -extldflags -static -s"
go build -gcflags "all=-N -l" -ldflags "-X main.VERSION=$VERSION $LINKFLAGS" -o bin/$(basename `pwd`)
cp -rf conf ./bin/


