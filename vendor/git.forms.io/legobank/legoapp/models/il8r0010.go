package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type IL8R0010IDataForm struct {
	FormHead CommonFormHead
	FormData IL8R0010I
}

type IL8R0010ODataForm struct {
	FormHead CommonFormHead
	FormData IL8R0010O
}

type IL8R0010RequestForm struct {
	Form []IL8R0010IDataForm
}

type IL8R0010ResponseForm struct {
	Form []IL8R0010ODataForm
}

type IL8R0010I struct {
	LoanDubilNo      				  string  `json:"LoanDubilNo,omitempty"`
	CustNo                            string  `json:"CustNo,omitempty"`
	LoanTotlPridnum                   int     `json:"LoanTotlPridnum,omitempty"`
	CurrExecPridnum                   int     `json:"CurrExecPridnum,omitempty"`
	LoanRemainPridnum                 int     `json:"LoanRemainPridnum,omitempty"`
	PrinDueBgnDt                      string  `json:"PrinDueBgnDt,omitempty"`
	IntrDueBgnDt                      string  `json:"IntrDueBgnDt,omitempty"`
	AccmDuePridnum                    int     `json:"AccmDuePridnum,omitempty"`
	AccmWrtoffAmt                     float64  `json:"AccmWrtoffAmt,omitempty"`
	LsttrmCompEtimLnRiskClsfDt        string  `json:"LsttrmCompEtimLnRiskClsfDt,omitempty"`
	LsttrmCompEtimLnRiskClsfCd        string  `json:"LsttrmCompEtimLnRiskClsfCd,omitempty"`
	CurtprdCompEtimLnRiskClsfDt       string  `json:"CurtprdCompEtimLnRiskClsfDt,omitempty"`
	CurtprdCompEtimLnRiskClsfCd       string  `json:"CurtprdCompEtimLnRiskClsfCd,omitempty"`
	LsttrmArtgclIdtfyLoanRiskClsfDt   string  `json:"LsttrmArtgclIdtfyLoanRiskClsfDt,omitempty"`
	LsttrmArtgclIdtfyLoanRiskClsfCd   string  `json:"LsttrmArtgclIdtfyLoanRiskClsfCd,omitempty"`
	CurtprdArtgclIdtfyLoanRiskClsfDt  string  `json:"CurtprdArtgclIdtfyLoanRiskClsfDt,omitempty"`
	CurtprdArtgclIdtfyLoanRiskClsfCd  string  `json:"CurtprdArtgclIdtfyLoanRiskClsfCd,omitempty"`
	FinlModfyDt                       string  `json:"FinlModfyDt,omitempty"`
	FinlModfyTm                       string  `json:"FinlModfyTm,omitempty"`
	FinlModfyOrgNo                    string  `json:"FinlModfyOrgNo,omitempty"`
	FinlModfyTelrNo                   string  `json:"FinlModfyTelrNo,omitempty"`
	PageNo                            int     `json:"PageNo,omitempty"`
	PageRecCount                      int     `json:"PageRecCount,omitempty"`
}

type IL8R0010O struct {
	Records                      []IL8R0010ORecords
	PageNo                       int  `json:"PageNo"`
	PageRecCount                 int  `json:"PageRecCount"`
}

type IL8R0010ORecords struct {
	LoanDubilNo      				  string  `json:"LoanDubilNo"`
	CustNo                            string  `json:"CustNo"`
	LoanTotlPridnum                   int     `json:"LoanTotlPridnum"`
	CurrExecPridnum                   int     `json:"CurrExecPridnum"`
	LoanRemainPridnum                 int     `json:"LoanRemainPridnum"`
	PrinDueBgnDt                      string  `json:"PrinDueBgnDt"`
	IntrDueBgnDt                      string  `json:"IntrDueBgnDt"`
	AccmDuePridnum                    int     `json:"AccmDuePridnum"`
	AccmWrtoffAmt                     float64  `json:"AccmWrtoffAmt"`
	LsttrmCompEtimLnRiskClsfDt        string  `json:"LsttrmCompEtimLnRiskClsfDt"`
	LsttrmCompEtimLnRiskClsfCd        string  `json:"LsttrmCompEtimLnRiskClsfCd"`
	CurtprdCompEtimLnRiskClsfDt       string  `json:"CurtprdCompEtimLnRiskClsfDt"`
	CurtprdCompEtimLnRiskClsfCd       string  `json:"CurtprdCompEtimLnRiskClsfCd"`
	LsttrmArtgclIdtfyLoanRiskClsfDt   string  `json:"LsttrmArtgclIdtfyLoanRiskClsfDt"`
	LsttrmArtgclIdtfyLoanRiskClsfCd   string  `json:"LsttrmArtgclIdtfyLoanRiskClsfCd"`
	CurtprdArtgclIdtfyLoanRiskClsfDt  string  `json:"CurtprdArtgclIdtfyLoanRiskClsfDt"`
	CurtprdArtgclIdtfyLoanRiskClsfCd  string  `json:"CurtprdArtgclIdtfyLoanRiskClsfCd"`
	FinlModfyDt                       string  `json:"FinlModfyDt"`
	FinlModfyTm                       string  `json:"FinlModfyTm"`
	FinlModfyOrgNo                    string  `json:"FinlModfyOrgNo"`
	FinlModfyTelrNo                   string  `json:"FinlModfyTelrNo"`
}

// @Desc Build request message
func (o *IL8R0010RequestForm) PackRequest(il8r0010I IL8R0010I) (responseBody []byte, err error) {

	requestForm := IL8R0010RequestForm{
		Form: []IL8R0010IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL8R0010I",
				},

				FormData: il8r0010I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL8R0010RequestForm) UnPackRequest(request []byte) (IL8R0010I, error) {
	il8r0010I := IL8R0010I{}
	if err := json.Unmarshal(request, o); nil != err {
		return il8r0010I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il8r0010I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL8R0010ResponseForm) PackResponse(il8r0010O IL8R0010O) (responseBody []byte, err error) {
	responseForm := IL8R0010ResponseForm{
		Form: []IL8R0010ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL8R0010O",
				},
				FormData: il8r0010O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL8R0010ResponseForm) UnPackResponse(request []byte) (IL8R0010O, error) {

	il8r0010O := IL8R0010O{}

	if err := json.Unmarshal(request, o); nil != err {
		return il8r0010O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il8r0010O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}
