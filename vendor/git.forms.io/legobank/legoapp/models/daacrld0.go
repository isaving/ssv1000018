package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLD0I struct {
	HostTranSerialNo	string ` validate:"required,max=34"`
	HostTranSeq			string ` validate:"required,max=34"`
	HostTranInq			string ` validate:"required,max=6"`
	SetNo				string
}

type DAACRLD0O struct {
	Abstract          string      `json:"Abstract"`
	AbstractCode      string      `json:"AbstractCode"`
	AccessChnlTypCd   string      `json:"AccessChnlTypCd"`
	AcctBal           int         `json:"AcctBal"`
	AcctBookNo        string      `json:"AcctBookNo"`
	AgenOrgNo         string      `json:"AgenOrgNo"`
	AuthTelrNo        string      `json:"AuthTelrNo"`
	BizClsfCd         string      `json:"BizClsfCd"`
	BizKindCd         string      `json:"BizKindCd"`
	BizSysNo          string      `json:"BizSysNo"`
	CardNoOrAcctNo    string      `json:"CardNoOrAcctNo"`
	CashTxRcptpymtCd  string      `json:"CashTxRcptpymtCd"`
	CashrmtFlgCd      string      `json:"CashrmtFlgCd"`
	ChnlRvrsCtrlFlgCd string      `json:"ChnlRvrsCtrlFlgCd"`
	CurCd             string      `json:"CurCd"`
	CustMgrTelrNo     string      `json:"CustMgrTelrNo"`
	CustNo            string      `json:"CustNo"`
	DebitCrdtFlg      string      `json:"DebitCrdtFlg"`
	FinTxAmtTypCd     string      `json:"FinTxAmtTypCd"`
	FrnexcgStlManrCd  string      `json:"FrnexcgStlManrCd"`
	GvayLiqdFlg       string      `json:"GvayLiqdFlg"`
	HostTranInq       string      `json:"HostTranInq"`
	HostTranSeq       string      `json:"HostTranSeq"`
	HostTranSerialNo  string      `json:"HostTranSerialNo"`
	LastMaintBrno     string      `json:"LastMaintBrno"`
	LastMaintDate     string      `json:"LastMaintDate"`
	LastMaintTell     string      `json:"LastMaintTell"`
	LastMaintTime     string      `json:"LastMaintTime"`
	LiqdBizTypCd      string      `json:"LiqdBizTypCd"`
	LiqdDt            string      `json:"LiqdDt"`
	LiqdTm            string      `json:"LiqdTm"`
	LoanAcctiAcctNo   string      `json:"LoanAcctiAcctNo"`
	LoanUsageCd       string      `json:"LoanUsageCd"`
	LunchChnlTypCd    string      `json:"LunchChnlTypCd"`
	MbankAcctFlg      string      `json:"MbankAcctFlg"`
	MbankFlg          string      `json:"MbankFlg"`
	MerchtNo          string      `json:"MerchtNo"`
	MessageCode       string      `json:"MessageCode"`
	MndArapFlg        string      `json:"MndArapFlg"`
	OpenAcctOrgNo     string      `json:"OpenAcctOrgNo"`
	OrginlHostTxSn    string      `json:"OrginlHostTxSn"`
	OrginlTxAcctnDt   string      `json:"OrginlTxAcctnDt"`
	OrginlTxTelrNo    string      `json:"OrginlTxTelrNo"`
	OthbnkBnkNo       string      `json:"OthbnkBnkNo"`
	PmitRvrsFlg       string      `json:"PmitRvrsFlg"`
	PostvReblnTxFlgCd string      `json:"PostvReblnTxFlgCd"`
	RchkTelrNo        string      `json:"RchkTelrNo"`
	RcrdacctAcctnDt   string      `json:"RcrdacctAcctnDt"`
	RcrdacctAcctnTm   string      `json:"RcrdacctAcctnTm"`
	RvrsTxFlgCd       string      `json:"RvrsTxFlgCd"`
	StateAndRgnCd     string      `json:"StateAndRgnCd"`
	Status            int         `json:"Status"`
	SvngAcctNo        string      `json:"SvngAcctNo"`
	TccState          interface{} `json:"TccState"`
	TerminalNo        string      `json:"TerminalNo"`
	TranCd            string      `json:"TranCd"`
	TxAcctnDt         string      `json:"TxAcctnDt"`
	TxDtlTypCd        string      `json:"TxDtlTypCd"`
	TxOrgNo           string      `json:"TxOrgNo"`
	TxTelrNo          string      `json:"TxTelrNo"`
	TxTm              string      `json:"TxTm"`
	TxnAmt            int         `json:"TxnAmt"`
	ValueDate         string      `json:"ValueDate"`
}

type DAACRLD0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLD0I
}

type DAACRLD0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLD0O
}

type DAACRLD0RequestForm struct {
	Form []DAACRLD0IDataForm
}

type DAACRLD0ResponseForm struct {
	Form []DAACRLD0ODataForm
}

// @Desc Build request message
func (o *DAACRLD0RequestForm) PackRequest(DAACRLD0I DAACRLD0I) (responseBody []byte, err error) {

	requestForm := DAACRLD0RequestForm{
		Form: []DAACRLD0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLD0I",
				},
				FormData: DAACRLD0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLD0RequestForm) UnPackRequest(request []byte) (DAACRLD0I, error) {
	DAACRLD0I := DAACRLD0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLD0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLD0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLD0ResponseForm) PackResponse(DAACRLD0O DAACRLD0O) (responseBody []byte, err error) {
	responseForm := DAACRLD0ResponseForm{
		Form: []DAACRLD0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLD0O",
				},
				FormData: DAACRLD0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLD0ResponseForm) UnPackResponse(request []byte) (DAACRLD0O, error) {

	DAACRLD0O := DAACRLD0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLD0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLD0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLD0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
