package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCLP0I struct {
	AcctgAcctNo              string ` validate:"required,max=20"`
	PeriodNum                int64
	PeriodNum1               int64
	AcctgDate                string
	CurrentStatus            string
	CurrPeriodIntacrBgnDt    string
	CurrPeriodIntacrEndDt    string
	ValueDate                string
	BurningSum               float64
	CurrPeriodRepayDt        string
	RepayStatus              string
	PlanRepayBal             float64
	ActualRepayDate          string
	ActualRepayBal           float64
	CurrentPeriodUnStillPrin float64
	LastMaintDate            string
	LastMaintTime            string
	LastMaintBrno            string
	LastMaintTell            string
}

type DAACCLP0O struct {
	AcctgAcctNo              string ` validate:"required,max=20"`
	PeriodNum                int64
	PeriodNum1               int64
	AcctgDate                string
	CurrentStatus            string
	CurrPeriodIntacrBgnDt    string
	CurrPeriodIntacrEndDt    string
	ValueDate                string
	BurningSum               float64
	CurrPeriodRepayDt        string
	RepayStatus              string
	PlanRepayBal             float64
	ActualRepayDate          string
	ActualRepayBal           float64
	CurrentPeriodUnStillPrin float64
	LastMaintDate            string
	LastMaintTime            string
	LastMaintBrno            string
	LastMaintTell            string
}

type DAACCLP0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCLP0I
}

type DAACCLP0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCLP0O
}

type DAACCLP0RequestForm struct {
	Form []DAACCLP0IDataForm
}

type DAACCLP0ResponseForm struct {
	Form []DAACCLP0ODataForm
}

// @Desc Build request message
func (o *DAACCLP0RequestForm) PackRequest(DAACCLP0I DAACCLP0I) (responseBody []byte, err error) {

	requestForm := DAACCLP0RequestForm{
		Form: []DAACCLP0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLP0I",
				},
				FormData: DAACCLP0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCLP0RequestForm) UnPackRequest(request []byte) (DAACCLP0I, error) {
	DAACCLP0I := DAACCLP0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLP0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLP0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCLP0ResponseForm) PackResponse(DAACCLP0O DAACCLP0O) (responseBody []byte, err error) {
	responseForm := DAACCLP0ResponseForm{
		Form: []DAACCLP0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCLP0O",
				},
				FormData: DAACCLP0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCLP0ResponseForm) UnPackResponse(request []byte) (DAACCLP0O, error) {

	DAACCLP0O := DAACCLP0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCLP0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCLP0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCLP0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
