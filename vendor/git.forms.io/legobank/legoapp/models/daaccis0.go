package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCIS0I struct {
	AcctgAcctNo   string
	IntType       string
	WorkDate      string
	BizFolnNo     string ` validate:"required,max=32"`
	SysFolnNo     string ` validate:"required,max=32"`
	DealCode      string
	Status        string
	CalcBgnDate   string
	CalcEndData   string
	CalcDays      int64
	CalcIntAmt    float64
	CalcIntRate   float64
	IntAmt        float64
	PmitRvrsFlg   string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	RecordNo      int64
}

type DAACCIS0O struct {
	AcctgAcctNo   string
	IntType       string
	WorkDate      string
	BizFolnNo     string
	SysFolnNo     string
	DealCode      string
	Status        string
	CalcBgnDate   string
	CalcEndData   string
	CalcDays      int64
	CalcIntAmt    float64
	CalcIntRate   float64
	IntAmt        float64
	PmitRvrsFlg   string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	RecordNo      int64
}

type DAACCIS0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCIS0I
}

type DAACCIS0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCIS0O
}

type DAACCIS0RequestForm struct {
	Form []DAACCIS0IDataForm
}

type DAACCIS0ResponseForm struct {
	Form []DAACCIS0ODataForm
}

// @Desc Build request message
func (o *DAACCIS0RequestForm) PackRequest(DAACCIS0I DAACCIS0I) (responseBody []byte, err error) {

	requestForm := DAACCIS0RequestForm{
		Form: []DAACCIS0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCIS0I",
				},
				FormData: DAACCIS0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCIS0RequestForm) UnPackRequest(request []byte) (DAACCIS0I, error) {
	DAACCIS0I := DAACCIS0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCIS0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCIS0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCIS0ResponseForm) PackResponse(DAACCIS0O DAACCIS0O) (responseBody []byte, err error) {
	responseForm := DAACCIS0ResponseForm{
		Form: []DAACCIS0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCIS0O",
				},
				FormData: DAACCIS0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCIS0ResponseForm) UnPackResponse(request []byte) (DAACCIS0O, error) {

	DAACCIS0O := DAACCIS0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCIS0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCIS0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCIS0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
