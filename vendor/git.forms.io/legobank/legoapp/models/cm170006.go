package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAB00001I struct {
	SysSts    string `json:"SysSts";valid:Required` //系统状态：O-日间服务；F-日终服务
	BatchDate string `json:"BatchDate,omitempty"`   //受控日切时传送该日期
}

type DAB00001O struct {
	Status string //执行状态
}

type DAB00001IDataForm struct {
	FormHead CommonFormHead
	FormData DAB00001I
}

type DAB00001ODataForm struct {
	FormHead CommonFormHead
	FormData DAB00001O
}

type DAB00001RequestForm struct {
	Form []DAB00001IDataForm
}

type DAB00001ResponseForm struct {
	Form []DAB00001ODataForm
}

// @Desc Build request message
func (o *DAB00001RequestForm) PackRequest(DAB00001I DAB00001I) (responseBody []byte, err error) {

	requestForm := DAB00001RequestForm{
		Form: []DAB00001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAB00001I",
				},
				FormData: DAB00001I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAB00001RequestForm) UnPackRequest(request []byte) (DAB00001I, error) {
	DAB00001I := DAB00001I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAB00001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAB00001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAB00001ResponseForm) PackResponse(DAB00001O DAB00001O) (responseBody []byte, err error) {
	responseForm := DAB00001ResponseForm{
		Form: []DAB00001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAB00001O",
				},
				FormData: DAB00001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAB00001ResponseForm) UnPackResponse(request []byte) (DAB00001O, error) {

	DAB00001O := DAB00001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAB00001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAB00001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAB00001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
