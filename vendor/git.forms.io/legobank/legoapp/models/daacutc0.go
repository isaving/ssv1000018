package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUTC0I struct {
	//输入是个map
}

type DAACUTC0O struct {

}

type DAACUTC0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUTC0I
}

type DAACUTC0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUTC0O
}

type DAACUTC0RequestForm struct {
	Form []DAACUTC0IDataForm
}

type DAACUTC0ResponseForm struct {
	Form []DAACUTC0ODataForm
}

// @Desc Build request message
func (o *DAACUTC0RequestForm) PackRequest(DAACUTC0I DAACUTC0I) (responseBody []byte, err error) {

	requestForm := DAACUTC0RequestForm{
		Form: []DAACUTC0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUTC0I",
				},
				FormData: DAACUTC0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUTC0RequestForm) UnPackRequest(request []byte) (DAACUTC0I, error) {
	DAACUTC0I := DAACUTC0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUTC0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUTC0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUTC0ResponseForm) PackResponse(DAACUTC0O DAACUTC0O) (responseBody []byte, err error) {
	responseForm := DAACUTC0ResponseForm{
		Form: []DAACUTC0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUTC0O",
				},
				FormData: DAACUTC0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUTC0ResponseForm) UnPackResponse(request []byte) (DAACUTC0O, error) {

	DAACUTC0O := DAACUTC0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUTC0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUTC0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUTC0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
