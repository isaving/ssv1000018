package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACB002I struct {
	AcctgAcctNo string `valid:"Required;MaxSize(20)"` //贷款核算账号
	DealDate    string `valid:"Required"`
}

type DAACB002O struct {
	BalanceType   string
	PeriodNum     int
	CalcBgnDate   string
	CalcEndData   string
	LastCalcDate  string
	AccmWdAmt     float64
	AcruUnstlIntr float64
	Status        string
}

type DAACB002IDataForm struct {
	FormHead CommonFormHead
	FormData DAACB002I
}

type DAACB002ODataForm struct {
	FormHead CommonFormHead
	FormData DAACB002O
}

type DAACB002RequestForm struct {
	Form []DAACB002IDataForm
}

type DAACB002ResponseForm struct {
	Form []DAACB002ODataForm
}

// @Desc Build request message
func (o *DAACB002RequestForm) PackRequest(DAACB002I DAACB002I) (responseBody []byte, err error) {

	requestForm := DAACB002RequestForm{
		Form: []DAACB002IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACB002I",
				},
				FormData: DAACB002I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACB002RequestForm) UnPackRequest(request []byte) (DAACB002I, error) {
	DAACB002I := DAACB002I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACB002I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACB002I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACB002ResponseForm) PackResponse(DAACB002O DAACB002O) (responseBody []byte, err error) {
	responseForm := DAACB002ResponseForm{
		Form: []DAACB002ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACB002O",
				},
				FormData: DAACB002O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACB002ResponseForm) UnPackResponse(request []byte) (DAACB002O, error) {

	DAACB002O := DAACB002O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACB002O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACB002O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACB002I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
