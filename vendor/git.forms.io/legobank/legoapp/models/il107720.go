package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL107720I struct {
	//acctg_acct_no string `valid:Required` //核算账号
	LoanDubilNo string `valid:Required` //借据号
	OnlineBizDt string `valid:Required` //会计日期

}

type IL107720O struct {
	Status string //处理成功返回会"ok"
}

type IL107720IDataForm struct {
	FormHead CommonFormHead
	FormData IL107720I
}

type IL107720ODataForm struct {
	FormHead CommonFormHead
	FormData IL107720O
}

type IL107720RequestForm struct {
	Form []IL107720IDataForm
}

type IL107720ResponseForm struct {
	Form []IL107720ODataForm
}

// @Desc Build request message
func (o *IL107720RequestForm) PackRequest(IL107720I IL107720I) (responseBody []byte, err error) {

	requestForm := IL107720RequestForm{
		Form: []IL107720IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL107720I",
				},
				FormData: IL107720I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL107720RequestForm) UnPackRequest(request []byte) (IL107720I, error) {
	IL107720I := IL107720I{}
	if err := json.Unmarshal(request, o); nil != err {
		return IL107720I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL107720I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL107720ResponseForm) PackResponse(IL107720O IL107720O) (responseBody []byte, err error) {
	responseForm := IL107720ResponseForm{
		Form: []IL107720ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL107720O",
				},
				FormData: IL107720O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL107720ResponseForm) UnPackResponse(request []byte) (IL107720O, error) {

	IL107720O := IL107720O{}

	if err := json.Unmarshal(request, o); nil != err {
		return IL107720O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL107720O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL107720I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
