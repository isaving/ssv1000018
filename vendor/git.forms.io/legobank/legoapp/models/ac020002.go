package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020002I struct {
	Currency       string `valid:"Required;MaxSize(4)"`  //币种
	AcctCreateDate string `valid:"Required;MaxSize(10)"` //	开户日
	TranDate       string `valid:"Required;MaxSize(10)"` //最后动账日期
	MgmtOrgId      string `valid:"Required;MaxSize(4)"`  //管理行所号
	AcctingOrgId   string `valid:"Required;MaxSize(4)"`  //核算行所号
}

type AC020002RequestForm struct {
	Form []AC020002IDataForm `json:"Form"`
}

type AC020002IDataForm struct {
	FormHead CommonFormHead		`json:"FormHead"`
	FormData AC020002I		`json:"FormData"`
}



type AC020002O struct {
	Status	string		`json:"status"`
	//TODO add output fields
}

type AC020002ResponseForm struct {
	Form []AC020002ODataForm `json:"Form"`
}

type AC020002ODataForm struct {
	FormHead CommonFormHead `json:"FormHead"`
	FormData AC020002O      `json:"FormData"`
}

// @Desc Parsing request message
func (w *AC020002RequestForm) UnPackRequest(req []byte) (AC020002I, error) {
	AC020002I := AC020002I{}
	if err := json.Unmarshal(req, w); nil != err {
		return AC020002I, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return AC020002I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

// @Desc Build response message
func (w *AC020002ResponseForm) PackResponse(AC020002OData AC020002O) (res []byte, err error) {
	resForm := AC020002ResponseForm{
		Form: []AC020002ODataForm{
			AC020002ODataForm{
				FormHead: CommonFormHead{
					FormId: "AC020002O001",
				},
				FormData: AC020002OData,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Build request message
func (w *AC020002RequestForm) PackRequest(AC020002IData AC020002I) (res []byte, err error) {
	requestForm := AC020002RequestForm{
		Form: []AC020002IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020002I001",
				},
				FormData: AC020002IData,
			},
		},
	}

	rspBody, err := json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Parsing response message
func (w *AC020002ResponseForm) UnPackResponse(req []byte) (AC020002O, error) {
	AC020002O := AC020002O{}

	if err := json.Unmarshal(req, w); nil != err {
		return AC020002O, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return AC020002O, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

func (w *AC020002I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}