package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCCH0I struct {
	IntPlanNo     string `validate:"required,max=15"`
	IntCalcOption string `validate:"required,max=10"`
	EffectDate    string `validate:"required,max=10"`
	ExpDate       string
	ValidFlag     string
	Bk            string
	LastMaintDate string
	LastMaintTime int64
	LastMaintBrno string
	LastMaintTell string
}

type DAACCCH0O struct {
	IntPlanNo     string
	IntCalcOption string
	EffectDate    string
	ExpDate       string
	ValidFlag     string
	Bk            string
	LastMaintDate string
	LastMaintTime int64
	LastMaintBrno string
	LastMaintTell string
}

type DAACCCH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCCH0I
}

type DAACCCH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCCH0O
}

type DAACCCH0RequestForm struct {
	Form []DAACCCH0IDataForm
}

type DAACCCH0ResponseForm struct {
	Form []DAACCCH0ODataForm
}

// @Desc Build request message
func (o *DAACCCH0RequestForm) PackRequest(DAACCCH0I DAACCCH0I) (responseBody []byte, err error) {

	requestForm := DAACCCH0RequestForm{
		Form: []DAACCCH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCCH0I",
				},
				FormData: DAACCCH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCCH0RequestForm) UnPackRequest(request []byte) (DAACCCH0I, error) {
	DAACCCH0I := DAACCCH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCCH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCCH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCCH0ResponseForm) PackResponse(DAACCCH0O DAACCCH0O) (responseBody []byte, err error) {
	responseForm := DAACCCH0ResponseForm{
		Form: []DAACCCH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCCH0O",
				},
				FormData: DAACCCH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCCH0ResponseForm) UnPackResponse(request []byte) (DAACCCH0O, error) {

	DAACCCH0O := DAACCCH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCCH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCCH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCCH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
