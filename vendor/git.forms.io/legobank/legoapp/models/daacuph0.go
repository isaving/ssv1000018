package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUPH0I struct {
	//输入是个map
}

type DAACUPH0O struct {

}

type DAACUPH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUPH0I
}

type DAACUPH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUPH0O
}

type DAACUPH0RequestForm struct {
	Form []DAACUPH0IDataForm
}

type DAACUPH0ResponseForm struct {
	Form []DAACUPH0ODataForm
}

// @Desc Build request message
func (o *DAACUPH0RequestForm) PackRequest(DAACUPH0I DAACUPH0I) (responseBody []byte, err error) {

	requestForm := DAACUPH0RequestForm{
		Form: []DAACUPH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUPH0I",
				},
				FormData: DAACUPH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUPH0RequestForm) UnPackRequest(request []byte) (DAACUPH0I, error) {
	DAACUPH0I := DAACUPH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUPH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUPH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUPH0ResponseForm) PackResponse(DAACUPH0O DAACUPH0O) (responseBody []byte, err error) {
	responseForm := DAACUPH0ResponseForm{
		Form: []DAACUPH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUPH0O",
				},
				FormData: DAACUPH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUPH0ResponseForm) UnPackResponse(request []byte) (DAACUPH0O, error) {

	DAACUPH0O := DAACUPH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUPH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUPH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUPH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
