package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLA0I struct {
	AcctgAcctNo		string ` validate:"required,max=20"`
}

type DAACRLA0O struct { //输出不是记录数组
	AcctCreateDate     string  `json:"AcctCreateDate"`
	AcctgAcctNo        string  `json:"AcctgAcctNo"`
	AcctgStatus        string  `json:"AcctgStatus"`
	AcctingOrgID       string  `json:"AcctingOrgId"`
	BalanceType        string  `json:"BalanceType"`
	Currency           string  `json:"Currency"`
	FrzAmt             float64 `json:"FrzAmt"`
	HoliCalcIntFlag    string  `json:"HoliCalcIntFlag"`
	IntCyc             string  `json:"IntCyc"`
	IntFreq            int     `json:"IntFreq"`
	IntSetlCyc         string  `json:"IntSetlCyc"`
	IntSetlFreq        int     `json:"IntSetlFreq"`
	IntSetlPreFlag     string  `json:"IntSetlPreFlag"`
	IntSetlSpeDate     string  `json:"IntSetlSpeDate"`
	IntSpeDate         string  `json:"IntSpeDate"`
	IsCalcDupInt       string  `json:"IsCalcDupInt"`
	IsCalcInt          string  `json:"IsCalcInt"`
	LastCalcIntDate    string  `json:"LastCalcIntDate"`
	LastMaintBrno      string  `json:"LastMaintBrno"`
	LastMaintDate      string  `json:"LastMaintDate"`
	LastMaintTell      string  `json:"LastMaintTell"`
	LastMaintTime      string  `json:"LastMaintTime"`
	LastTranDate       string  `json:"LastTranDate"`
	LastTranIntDate    string  `json:"LastTranIntDate"`
	LastWdIntDate      string  `json:"LastWdIntDate"`
	MgmtOrgID          string  `json:"MgmtOrgId"`
	OrgCreate          string  `json:"OrgCreate"`
	PmitBkvtFlag       string  `json:"PmitBkvtFlag"`
	PmitDlydBegintFlag string  `json:"PmitDlydBegintFlag"`
	TccState           int     `json:"TccState"`
	WdCyc              string  `json:"WdCyc"`
	WdFreq             int     `json:"WdFreq"`
	WdIntFlag          string  `json:"WdIntFlag"`
}

type DAACRLA0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLA0I
}

type DAACRLA0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLA0O
}

type DAACRLA0RequestForm struct {
	Form []DAACRLA0IDataForm
}

type DAACRLA0ResponseForm struct {
	Form []DAACRLA0ODataForm
}

// @Desc Build request message
func (o *DAACRLA0RequestForm) PackRequest(DAACRLA0I DAACRLA0I) (responseBody []byte, err error) {

	requestForm := DAACRLA0RequestForm{
		Form: []DAACRLA0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLA0I",
				},
				FormData: DAACRLA0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLA0RequestForm) UnPackRequest(request []byte) (DAACRLA0I, error) {
	DAACRLA0I := DAACRLA0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLA0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLA0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLA0ResponseForm) PackResponse(DAACRLA0O DAACRLA0O) (responseBody []byte, err error) {
	responseForm := DAACRLA0ResponseForm{
		Form: []DAACRLA0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLA0O",
				},
				FormData: DAACRLA0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLA0ResponseForm) UnPackResponse(request []byte) (DAACRLA0O, error) {

	DAACRLA0O := DAACRLA0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLA0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLA0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLA0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
