package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACULP0I struct {
	//输入是个map
}

type DAACULP0O struct {

}

type DAACULP0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACULP0I
}

type DAACULP0ODataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type DAACULP0RequestForm struct {
	Form []DAACULP0IDataForm
}

type DAACULP0ResponseForm struct {
	Form []DAACULP0ODataForm
}

// @Desc Build request message
func (o *DAACULP0RequestForm) PackRequest(DAACULP0I DAACULP0I) (responseBody []byte, err error) {

	requestForm := DAACULP0RequestForm{
		Form: []DAACULP0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULP0I",
				},
				FormData: DAACULP0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACULP0RequestForm) UnPackRequest(request []byte) (DAACULP0I, error) {
	DAACULP0I := DAACULP0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACULP0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULP0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACULP0ResponseForm) PackResponse(DAACULP0O map[string]interface{}) (responseBody []byte, err error) {
	responseForm := DAACULP0ResponseForm{
		Form: []DAACULP0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULP0O",
				},
				FormData: DAACULP0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACULP0ResponseForm) UnPackResponse(request []byte) (map[string]interface{}, error) {

	DAACULP0O := make(map[string]interface{})

	if err := json.Unmarshal(request, o); nil != err {
		return DAACULP0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULP0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACULP0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
