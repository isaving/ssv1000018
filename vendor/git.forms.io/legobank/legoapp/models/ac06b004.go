package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC06B004I struct {
	AcctgAcctNo     string  `valid:"Required;MaxSize(32)"` //贷款核算账号
	BeginDt         string  `valid:"Required;MaxSize(10)"` //计提开始时间	String(10)  结息优先类型代码1必传（还款分期表的计息开始日期）
	EndDt           string  `valid:"Required;MaxSize(10)"` //计提结束时间	String(10)  结息优先类型代码1必传（还款分期表的计息结束日期）
	IntStlPrtyTypCd string  `valid:"Required;MaxSize(1)"`  //结息优先类型代码	String(1)  1-以计划还息金额为结息金额
	PeriodNum       int     `valid:"Required"`             //期数	Numeric(11)  结息优先类型代码1必传
	PlanRpyIntAmt   float64 //应还利息金额	Numeric(18,2)  结息优先类型代码1必传
	IntDir          string  //利息方向
}

type AC06B004O struct {
	AcctgAcctNo string
	Records     []AC06B004Orecords
}

type AC06B004Orecords struct {
	PeriodNum            string
	IntrStusCd           int
	BegintD              string
	EndDt                string
	PlanRpyintAmt        float64 //应还利息金额	Numeric(18,2)  结息优先类型代码1必传
	ActlRpyintAmt        float64
	AccmCnDwAmt          float64
	AccmIntStlAmt        float64
	RecvblCmpdAmt        float64
	ActlRecvCmpdAmt      float64
	AlrdyTranOffshetIntr float64
	OnshetIntr           float64
	WrtoffIntrAmt        float64
	WrtoffIntacrAmt      float64
	Status               int
	FrzAmt               float64
	LastCalcDate         string
	AddCnDwAmt           float64
	CurrIntStDate        string

}

type AC06B004IDataForm struct {
	FormHead CommonFormHead
	FormData AC06B004I
}

type AC06B004ODataForm struct {
	FormHead CommonFormHead
	FormData AC06B004O
}

type AC06B004RequestForm struct {
	Form []AC06B004IDataForm
}

type AC06B004ResponseForm struct {
	Form []AC06B004ODataForm
}

// @Desc Build request message
func (o *AC06B004RequestForm) PackRequest(AC06B004I AC06B004I) (responseBody []byte, err error) {

	requestForm := AC06B004RequestForm{
		Form: []AC06B004IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC06B004I",
				},
				FormData: AC06B004I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC06B004RequestForm) UnPackRequest(request []byte) (AC06B004I, error) {
	AC06B004I := AC06B004I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC06B004I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC06B004I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC06B004ResponseForm) PackResponse(AC06B004O AC06B004O) (responseBody []byte, err error) {
	responseForm := AC06B004ResponseForm{
		Form: []AC06B004ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC06B004O",
				},
				FormData: AC06B004O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC06B004ResponseForm) UnPackResponse(request []byte) (AC06B004O, error) {

	AC06B004O := AC06B004O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC06B004O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC06B004O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC06B004I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
