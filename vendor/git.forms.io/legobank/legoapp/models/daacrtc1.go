package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRTC1I struct {
	AcctgAcctNo	string `validate:"required,max=40"`
}

type DAACRTC1O struct {
	LogTotCount string
	Records []DAACRTC3ORecords
}


type DAACRTC1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRTC1I
}

type DAACRTC1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRTC1O
}

type DAACRTC1RequestForm struct {
	Form []DAACRTC1IDataForm
}

type DAACRTC1ResponseForm struct {
	Form []DAACRTC1ODataForm
}

// @Desc Build request message
func (o *DAACRTC1RequestForm) PackRequest(DAACRTC1I DAACRTC1I) (responseBody []byte, err error) {

	requestForm := DAACRTC1RequestForm{
		Form: []DAACRTC1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTC1I",
				},
				FormData: DAACRTC1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRTC1RequestForm) UnPackRequest(request []byte) (DAACRTC1I, error) {
	DAACRTC1I := DAACRTC1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTC1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTC1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRTC1ResponseForm) PackResponse(DAACRTC1O DAACRTC1O) (responseBody []byte, err error) {
	responseForm := DAACRTC1ResponseForm{
		Form: []DAACRTC1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTC1O",
				},
				FormData: DAACRTC1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRTC1ResponseForm) UnPackResponse(request []byte) (DAACRTC1O, error) {

	DAACRTC1O := DAACRTC1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTC1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTC1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRTC1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
