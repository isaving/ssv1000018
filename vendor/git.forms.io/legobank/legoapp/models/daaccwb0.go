package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCWB0I struct {
	AcctgAcctNo   string
	Status        string
	Currency      string
	WorkDate      string
	MgmtOrgId     string
	AcctingOrgId  string
	BizFolnNo     string `validate:"required,max=32"`
	SysFolnNo     string `validate:"required,max=32"`
	SeqNo         int
	ChnlType      string
	DealType      string
	CalcBgnDate   string
	CalcEndData   string
	CalcDays      int64
	CalcPrimInt   float64
	BalanceType   string
	IntPlanNo     string
	CalcIntRate   float64
	CalcPattern   string
	DealStatus    string
	MsgCode       string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	PeriodNum     int64
}

type DAACCWB0O struct {
	AcctgAcctNo   string
	Status        string
	Currency      string
	WorkDate      string
	MgmtOrgId     string
	AcctingOrgId  string
	BizFolnNo     string
	SysFolnNo     string
	SeqNo         int
	ChnlType      string
	DealType      string
	CalcBgnDate   string
	CalcEndData   string
	CalcDays      int64
	CalcPrimInt   float64
	BalanceType   string
	IntPlanNo     string
	CalcIntRate   float64
	CalcPattern   string
	DealStatus    string
	MsgCode       string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	PeriodNum     int64

}

type DAACCWB0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCWB0I
}

type DAACCWB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCWB0O
}

type DAACCWB0RequestForm struct {
	Form []DAACCWB0IDataForm
}

type DAACCWB0ResponseForm struct {
	Form []DAACCWB0ODataForm
}

// @Desc Build request message
func (o *DAACCWB0RequestForm) PackRequest(DAACCWB0I DAACCWB0I) (responseBody []byte, err error) {

	requestForm := DAACCWB0RequestForm{
		Form: []DAACCWB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCWB0I",
				},
				FormData: DAACCWB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCWB0RequestForm) UnPackRequest(request []byte) (DAACCWB0I, error) {
	DAACCWB0I := DAACCWB0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCWB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCWB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCWB0ResponseForm) PackResponse(DAACCWB0O DAACCWB0O) (responseBody []byte, err error) {
	responseForm := DAACCWB0ResponseForm{
		Form: []DAACCWB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCWB0O",
				},
				FormData: DAACCWB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCWB0ResponseForm) UnPackResponse(request []byte) (DAACCWB0O, error) {

	DAACCWB0O := DAACCWB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCWB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCWB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCWB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
