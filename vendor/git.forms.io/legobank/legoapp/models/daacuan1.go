package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUAN1I struct {
	//输入是个map
}

type DAACUAN1O struct {

}

type DAACUAN1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUAN1I
}

type DAACUAN1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUAN1O
}

type DAACUAN1RequestForm struct {
	Form []DAACUAN1IDataForm
}

type DAACUAN1ResponseForm struct {
	Form []DAACUAN1ODataForm
}

// @Desc Build request message
func (o *DAACUAN1RequestForm) PackRequest(DAACUAN1I DAACUAN1I) (responseBody []byte, err error) {

	requestForm := DAACUAN1RequestForm{
		Form: []DAACUAN1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUAN1I",
				},
				FormData: DAACUAN1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUAN1RequestForm) UnPackRequest(request []byte) (DAACUAN1I, error) {
	DAACUAN1I := DAACUAN1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUAN1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUAN1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUAN1ResponseForm) PackResponse(DAACUAN1O DAACUAN1O) (responseBody []byte, err error) {
	responseForm := DAACUAN1ResponseForm{
		Form: []DAACUAN1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUAN1O",
				},
				FormData: DAACUAN1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUAN1ResponseForm) UnPackResponse(request []byte) (DAACUAN1O, error) {

	DAACUAN1O := DAACUAN1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUAN1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUAN1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUAN1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
