package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC060006I struct {
	AcctgAcctNo string        `valid:"Required;MaxSize(20)"` //贷款核算账号
	QueryWay    string        `valid:"Required"`             //查询方式 （1-查询所有期次；2-查询固定期次）
	BalanceType string        //利息状态
	RecordsNum  int           //数组记录条数
	Records     []DAACRTC3IRecords //数组
}

/*type Msg struct {
	AcctgAcctNo string
	PeriodNum   int
}*/

type AC060006O struct {
	AcctCreateDate     string
	AcctgAcctNo        string
	AcctgStatus        string
	AcctingOrgId       string
	BalanceType        string
	Currency           string
	FrzAmt             float64
	HoliCalcIntFlag    string
	IntCyc             string
	IntFreq            int
	IntSetlCyc         string
	IntSetlFreq        int
	IntSetlPreFlag     string
	IntSetlSpeDate     string
	IntSpeDate         string
	IsCalcDupInt       string
	IsCalcInt          string
	LastCalcIntDate    string
	LastMaintBrno      string
	LastMaintDate      string
	LastMaintTell      string
	LastMaintTime      string
	LastTranDate       string
	LastTranIntDate    string
	LastWdIntDate      string
	MgmtOrgId          string
	OrgCreate          string
	PmitBkvtFlag       string
	PmitDlydBegintFlag string
	Records            []DAACRTC3ORecords
}
/*type records struct {
	AccmCmpdAmt         float64
	AccmIntSetlAmt      float64
	AccmWdAmt           float64
	AcctgAcctNo         string
	AcruUnstlIntr       float64
	AlrdyTranOffshetInt float64
	BalanceType         string
	CavInt              float64
	CurrIntEndDate      string
	CurrIntStDate       string
	DcValueInt          float64
	FrzAmt              float64
	LastCalcDate        string
	LastMaintBrno       string
	LastMaintDate       string
	LastMaintTell       string
	LastMaintTime       string
	OnshetInt           float64
	PeriodNum           int
	RecordNo            int
	RepaidInt           float64
	RepaidIntAmt        float64
	Status              string
	TccState            int
	UnpaidInt           float64
	UnpaidIntAmt        float64
}*/

type AC060006IDataForm struct {
	FormHead CommonFormHead
	FormData AC060006I
}

type AC060006ODataForm struct {
	FormHead CommonFormHead
	FormData AC060006O
}

type AC060006RequestForm struct {
	Form []AC060006IDataForm
}

type AC060006ResponseForm struct {
	Form []AC060006ODataForm
}

// @Desc Build request message
func (o *AC060006RequestForm) PackRequest(AC060006I AC060006I) (responseBody []byte, err error) {

	requestForm := AC060006RequestForm{
		Form: []AC060006IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060006I",
				},
				FormData: AC060006I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC060006RequestForm) UnPackRequest(request []byte) (AC060006I, error) {
	AC060006I := AC060006I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC060006I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060006I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC060006ResponseForm) PackResponse(AC060006O AC060006O) (responseBody []byte, err error) {
	responseForm := AC060006ResponseForm{
		Form: []AC060006ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060006O",
				},
				FormData: AC060006O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC060006ResponseForm) UnPackResponse(request []byte) (AC060006O, error) {

	AC060006O := AC060006O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC060006O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060006O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC060006I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
