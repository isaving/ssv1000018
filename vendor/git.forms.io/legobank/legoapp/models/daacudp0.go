package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUDP0I struct {
	AcctgAcctNo           string ` validate:"required,max=20"`
	CustId                string
	ContId                string
	ProdCode              string
	OrgId                 string
	BanknoteFlag          string
	AcctType              string
	CustType              string
	DebitFlag             string
	IsAllowOverdraft      string
	Currency              string
	CurrencyMarket        string
	AcctStatus            string
	IsCalInt              string
	AcctOpenDate          string
	FrozenAmt             float64
	ReservedAmt           float64
	LastAcctBal           float64
	CurrentAcctBal        float64
	CurrentAcctBalTemp    float64
	SumCurrentBal         float64
	SumPeriodicBal        float64
	FirstDepDate          string
	LastSubmitDate        string
	CustLastEventDate     string
	LastEventDate         string
	LastRestDate          string
	LastRestBeginDate     string
	LastRestAmt           float64
	NextRestDate          string
	IntPaidAmt            float64
	LastAccruedDate       string
	NextAccrualDate       string
	RestPeriodAccruedDays float64
	AccruedIntAmt         float64
	LastTotDays           string
	DayDebitAmt           float64
	DayDebitCount         float64
	DayCreditAmt          float64
	DayCreditCount        float64
	DayCashInAmt          float64
	DayCashOutAmt         float64
	DayTranInAmt          float64
	DayTranOutAmt         float64
	DayTranCnt            float64
	LastMaintDate         string
	LastMaintTime         string
	LastMaintBrno         string
	LastMaintTell         string
	DeductFlag            string
	IntrPlanNo            string

	HostTranSerialNo          string
	BussDate                  string
	BussTime                  string
	HostTranSeq               int64
	PeripheralSysWorkday      string
	PeripheralSysWorktime     string
	PeripheralTranSerialNo    string
	PeripheralTranSeq         float64
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	FunctionCode              string
	ApprovalNo                string
	ClearingBussType          string
	ProdSeq                   float64
	BussType                  string
	BussCategories            string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	SubjectNo                 string
	SubjectBreakdown          string
	MediaType                 string
	LocalMediaPrefix          string
	MediaNo                   string
	CustDiff                  string
	AmtType                   string
	DurationOfDep             float64
	DaysOfDep                 float64
	AcctingCode1              string
	AcctingCode2              string
	AcctingCode3              string
	AcctingCode4              string
	EventIndication           string
	EventBreakdown            string
	CustEvent1                string
	CustEvent2                string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  float64
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
}

type DAACUDP0O struct {

}

type DAACUDP0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUDP0I
}

type DAACUDP0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUDP0O
}

type DAACUDP0RequestForm struct {
	Form []DAACUDP0IDataForm
}

type DAACUDP0ResponseForm struct {
	Form []DAACUDP0ODataForm
}

// @Desc Build request message
func (o *DAACUDP0RequestForm) PackRequest(DAACUDP0I DAACUDP0I) (responseBody []byte, err error) {

	requestForm := DAACUDP0RequestForm{
		Form: []DAACUDP0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUDP0I",
				},
				FormData: DAACUDP0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUDP0RequestForm) UnPackRequest(request []byte) (DAACUDP0I, error) {
	DAACUDP0I := DAACUDP0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUDP0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUDP0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUDP0ResponseForm) PackResponse(DAACUDP0O DAACUDP0O) (responseBody []byte, err error) {
	responseForm := DAACUDP0ResponseForm{
		Form: []DAACUDP0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUDP0O",
				},
				FormData: DAACUDP0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUDP0ResponseForm) UnPackResponse(request []byte) (DAACUDP0O, error) {

	DAACUDP0O := DAACUDP0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUDP0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUDP0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUDP0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
