package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type RetmsgForm struct {
	Form []RetmsgDataForm
}

type RetmsgDataForm struct {
	FormHead CommonFormHead `json:"FormHead"`
	FormData Retmsg         `json:"FormData"`
}

type Retmsg struct {
	RetMessage string `json:"RetMessage"`
	RetMsgCode string `json:"RetMsgCode"`
}

func (w *RetmsgForm) UnPackRequest(req []byte) (Retmsg, *errors.ServiceError) {
	RetmsgI := Retmsg{}
	if err := json.Unmarshal(req, w); nil != err {
		return RetmsgI, errors.Wrap(err, 0, "")
	}

	if len(w.Form) < 1 {
		return RetmsgI, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

// @Desc Build Request message
func (w *RetmsgForm) PackRequest(retmsg Retmsg) (res []byte, err error) {
	resForm := RetmsgForm{
		Form: []RetmsgDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "RETMSG",
				},
				FormData: retmsg,
			},
		},
	}
	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}
	return rspBody, nil
}
