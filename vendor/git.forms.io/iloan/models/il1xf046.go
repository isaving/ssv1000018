package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL1XF046IDataForm struct {
	FormHead CommonFormHead
	FormData IL1XF046I
}

type IL1XF046ODataForm struct {
	FormHead CommonFormHead
	FormData IL1XF046O
}

type IL1XF046RequestForm struct {
	Form []IL1XF046IDataForm
}

type IL1XF046ResponseForm struct {
	Form []IL1XF046ODataForm
}

type IL1XF046I struct {
	UserOrgNo   string `validate:"required",json:"UserOrgNo"`
	Empnbr      string `validate:"required",json:"Empnbr"`
}

type IL1XF046O struct {
	Status string `json:"Status"`
}

// @Desc Build request message
func (o *IL1XF046RequestForm) PackRequest(il1xf046I IL1XF046I) (responseBody []byte, err error) {

	requestForm := IL1XF046RequestForm{
		Form: []IL1XF046IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL1XF046I",
				},
				FormData: il1xf046I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL1XF046RequestForm) UnPackRequest(request []byte) (IL1XF046I, error) {
	il1xf046I := IL1XF046I{}
	if err := json.Unmarshal(request, o); nil != err {
		return il1xf046I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il1xf046I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL1XF046ResponseForm) PackResponse(il1xf046O IL1XF046O) (responseBody []byte, err error) {
	responseForm := IL1XF046ResponseForm{
		Form: []IL1XF046ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL1XF046O",
				},
				FormData: il1xf046O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL1XF046ResponseForm) UnPackResponse(request []byte) (IL1XF046O, error) {

	il1xf046O := IL1XF046O{}

	if err := json.Unmarshal(request, o); nil != err {
		return il1xf046O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il1xf046O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL1XF046I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
