package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL1XF047IDataForm struct {
	FormHead CommonFormHead
	FormData IL1XF047I
}

type IL1XF047ODataForm struct {
	FormHead CommonFormHead
	FormData IL1XF047O
}

type IL1XF047RequestForm struct {
	Form []IL1XF047IDataForm
}

type IL1XF047ResponseForm struct {
	Form []IL1XF047ODataForm
}

type IL1XF047I struct {
	InstId string `validate:"required",json:"InstId"`
}

type IL1XF047O struct {
	SystemStatus       string `json:"SystemStatus"`
	CurrentProcessDate string `json:"CurrentProcessDate"`
	LastProcessDate    string `json:"LastProcessDate"`
	NextProcessDate    string `json:"NextProcessDate"`
}

// @Desc Build request message
func (o *IL1XF047RequestForm) PackRequest(il1xf047I IL1XF047I) (responseBody []byte, err error) {

	requestForm := IL1XF047RequestForm{
		Form: []IL1XF047IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL1XF047I",
				},
				FormData: il1xf047I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL1XF047RequestForm) UnPackRequest(request []byte) (IL1XF047I, error) {
	il1xf047I := IL1XF047I{}
	if err := json.Unmarshal(request, o); nil != err {
		return il1xf047I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il1xf047I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL1XF047ResponseForm) PackResponse(il1xf047O IL1XF047O) (responseBody []byte, err error) {
	responseForm := IL1XF047ResponseForm{
		Form: []IL1XF047ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL1XF047O",
				},
				FormData: il1xf047O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL1XF047ResponseForm) UnPackResponse(request []byte) (IL1XF047O, error) {

	il1xf047O := IL1XF047O{}

	if err := json.Unmarshal(request, o); nil != err {
		return il1xf047O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il1xf047O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL1XF047I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
