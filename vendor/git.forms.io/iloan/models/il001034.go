package models

import (
	model "git.forms.io/customer"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

//TODO add constant message

type IL001034I struct {
	AcctManager  string
	OwnOrgId     string
	CustId       string
	IdType       string
	IdNo         string
	CustCountry  string
	PageNo       int
	PageRecCount int
}

type IL001034O struct {
	PageNo       int
	PageTotCount int
	Records      []model.DACURCM1O
}

type IL001034IDataForm struct {
	FormHead CommonFormHead
	FormData IL001034I
}

type IL001034ODataForm struct {
	FormHead CommonFormHead
	FormData IL001034O
}

type IL001034RequestForm struct {
	Form []IL001034IDataForm
}

type IL001034ResponseForm struct {
	Form []IL001034ODataForm
}

// @Desc Build request message
func (o *IL001034RequestForm) PackRequest(il001034I IL001034I) (requestBody []byte, err error) {

	requestForm := IL001034RequestForm{
		Form: []IL001034IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL001034I001",
				},
				FormData: il001034I,
			},
		},
	}

	requestBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *IL001034RequestForm) UnPackRequest(request []byte) (IL001034I, error) {
	il001034I := IL001034I{}
	if err := json.Unmarshal(request, o); nil != err {
		return il001034I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il001034I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL001034ResponseForm) PackResponse(il001034O IL001034O) (responseBody []byte, err error) {
	responseForm := IL001034ResponseForm{
		Form: []IL001034ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL001034O001",
				},
				FormData: il001034O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}


// @Desc Parsing response message
func (o *IL001034ResponseForm) UnPackResponse(request []byte) (IL001034O, error) {

	il001034O := IL001034O{}

	if err := json.Unmarshal(request, o); nil != err {
		return il001034O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il001034O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

