//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package base

import (
	"encoding/json"
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/common/event_handler/constant"
	"git.forms.io/universe/solapp-sdk/event/message"
	"github.com/astaxie/beego"
)

type BaseController struct {
	BaseHandler
	beego.Controller
}

func (b *BaseController) Prepare() {
	txnMessage := &message.TxnMessage{}
	if err := json.Unmarshal(b.Ctx.Input.RequestBody, txnMessage); nil == err {
		b.Req = &client.UserMessage{
			Id:             txnMessage.Id,
			AppProps:       txnMessage.AppProps,
			TopicAttribute: txnMessage.TopicAttribute,
			NeedReply:      txnMessage.NeedReply,
			NeedAck:        txnMessage.NeedReply,

			//message payload
			Body: []byte(txnMessage.Body),
		}
	}
}

func (b *BaseController) Finish() {
	txnMsg := &message.TxnMessage{}
	if nil != b.Rsp {
		txnMsg.Id = b.Rsp.Id
		txnMsg.TopicAttribute = b.Rsp.TopicAttribute
		txnMsg.NeedReply = b.Rsp.NeedReply
		txnMsg.NeedAck = b.Rsp.NeedAck
		txnMsg.AppProps = b.Rsp.AppProps

	}
	if nil == b.Rsp && b.Err != nil {
		errRes := CommonResponse{
			ErrorCode: -1,
			ErrorMsg:  b.Err.Error(),
		}
		errResJson, _ := json.Marshal(errRes)
		txnMsg.Body = string(errResJson)
		b.Data[constant.MESSAGE_TYPE_JSON] = *txnMsg
	} else {
		txnMsg.Body = string(b.Rsp.Body)
	}
	b.Data[constant.MESSAGE_TYPE_JSON] = *txnMsg
	b.ServeJSON()
}
