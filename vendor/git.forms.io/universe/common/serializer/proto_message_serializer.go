//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package serializer

import (
	"encoding/base64"
	"encoding/binary"
	"fmt"
	proto "git.forms.io/universe/comm-agent/common/protocol"
	"github.com/go-errors/errors"
	"github.com/intel-go/bytebuf"
)

var byteZero = byte(0)
var byteOne = byte(1)

var zeroErrorLength = []byte{0, 0, 0, 0}

var byteZeroArray = []byte{0}
var byteOneArray = []byte{1}

func bool2bytes(b bool) []byte {
	if b {
		return byteOneArray
	} else {
		return byteZeroArray
	}
}

func byte2bool(b byte) bool {
	if byteZero == b {
		return false
	} else {
		return true
	}
}

func map2String(m map[string]string) []byte {
	build := bytebuf.NewPointer()
	lenBytes := []byte{0, 0}
	for k, v := range m {
		keyBytes := []byte(k)
		valueBytes := []byte(v)
		lenKey := len(keyBytes)
		lenKeyValue := lenKey + len(valueBytes) + 1

		lenBytes[1] = byte(lenKeyValue)
		lenBytes[0] = byte(lenKeyValue >> 8)

		build.Write(lenBytes)         // 2 bytes:total length
		build.WriteByte(byte(lenKey)) // 1 byte:key length
		build.Write(keyBytes)         // bytes of key
		build.Write(valueBytes)       // bytes of value
	}
	len := build.Len()

	totalLenBytes := []byte{0, 0, 0, 0}
	totalLenBytes[3] = byte(len)
	totalLenBytes[2] = byte(len >> 8)
	totalLenBytes[1] = byte(len >> 16)
	totalLenBytes[0] = byte(len >> 24)

	result := append(totalLenBytes, build.Bytes()...)
	return result
}

func string2Map(inBytes []byte, totalLength uint32) map[string]string {
	var retMap map[string]string
	var tmpLen uint32

	retMap = make(map[string]string)
	for tmpLen = 0; tmpLen < totalLength; {
		keyLengthStartPos := tmpLen + 2
		keyLengthEndPos := tmpLen + 3
		pairLen := uint32(binary.BigEndian.Uint16([]byte(inBytes[tmpLen:keyLengthStartPos])))
		keyLength := uint32([]byte(inBytes[keyLengthStartPos:keyLengthEndPos])[0])
		keyEndPos := keyLengthEndPos + keyLength
		tmpLen = keyLengthStartPos + pairLen

		key := string(inBytes[keyLengthEndPos:keyEndPos])
		value := string(inBytes[keyEndPos:tmpLen])

		retMap[key] = value
	}

	return retMap
}

func String2ID(inputStr string) (uint64, error) {
	inputBytes, _ := base64.StdEncoding.DecodeString(inputStr)

	if len(inputBytes) < 8 {
		err := errors.Errorf("inputstr invalid:[%s]", string(inputBytes))
		//err := fmt.Errorf("inputstr invalid:[%s]", string(inputBytes))
		//log.Errorf("String2ID failed,:[%v]", err)
		return 0, err
	}

	return binary.BigEndian.Uint64(inputBytes), nil
}

func ID2String(id uint64) string {
	// id
	idBytes := []byte{0, 0, 0, 0, 0, 0, 0, 0}
	idBytes[7] = byte(id)
	idBytes[6] = byte(id >> 8)
	idBytes[5] = byte(id >> 16)
	idBytes[4] = byte(id >> 24)
	idBytes[3] = byte(id >> 32)
	idBytes[2] = byte(id >> 40)
	idBytes[1] = byte(id >> 48)
	idBytes[0] = byte(id >> 56)

	return base64.StdEncoding.EncodeToString(idBytes)
}

func Error2String(err error) string {
	build := bytebuf.NewPointer()
	if nil == err {
		build.Write(byteZeroArray)
		build.Write(zeroErrorLength)
	} else {
		build.Write(byteOneArray)
		errStr := err.Error()
		errBytes := []byte(errStr)
		lengthOfErr := len(errBytes)

		lengthOfErrBytes := []byte{0, 0, 0, 0}
		lengthOfErrBytes[3] = byte(lengthOfErr)
		lengthOfErrBytes[2] = byte(lengthOfErr >> 8)
		lengthOfErrBytes[1] = byte(lengthOfErr >> 16)
		lengthOfErrBytes[0] = byte(lengthOfErr >> 24)

		build.Write(lengthOfErrBytes)
		build.Write(errBytes)

	}
	return base64.StdEncoding.EncodeToString(build.Bytes())
}

func String2Error(inputStr string) error {
	inputBytes, _ := base64.StdEncoding.DecodeString(inputStr)
	if len(inputBytes) < 1 {
		err := errors.Errorf("inputstr invalid:[%s]", string(inputBytes))
		//err := fmt.Errorf("inputstr invalid:[%s]", string(inputBytes))
		//log.Errorf("String2Error failed,:[%v]", err)
		return err
	}

	if errFlag := inputBytes[0]; byteOne == errFlag {
		errLength := binary.BigEndian.Uint32(inputBytes[1:5])
		errorMessage := string(inputBytes[5 : 5+errLength])
		return fmt.Errorf(errorMessage)
	} else {
		return nil
	}
}

func ProtoMsg2String(msg *proto.Message, err error) string {
	//var build strings.Builder
	build := bytebuf.NewPointer()
	if nil == err {
		build.Write(byteZeroArray)
		build.Write(zeroErrorLength)
	} else {
		build.Write(byteOneArray)
		errStr := err.Error()
		errBytes := []byte(errStr)
		lengthOfErr := len(errBytes)

		lengthOfErrBytes := []byte{0, 0, 0, 0}
		lengthOfErrBytes[3] = byte(lengthOfErr)
		lengthOfErrBytes[2] = byte(lengthOfErr >> 8)
		lengthOfErrBytes[1] = byte(lengthOfErr >> 16)
		lengthOfErrBytes[0] = byte(lengthOfErr >> 24)

		build.Write(lengthOfErrBytes)
		build.Write(errBytes)

		return base64.StdEncoding.EncodeToString(build.Bytes())
	}

	// id
	idBytes := []byte{0, 0, 0, 0, 0, 0, 0, 0}
	idBytes[7] = byte(msg.Id)
	idBytes[6] = byte(msg.Id >> 8)
	idBytes[5] = byte(msg.Id >> 16)
	idBytes[4] = byte(msg.Id >> 24)
	idBytes[3] = byte(msg.Id >> 32)
	idBytes[2] = byte(msg.Id >> 40)
	idBytes[1] = byte(msg.Id >> 48)
	idBytes[0] = byte(msg.Id >> 56)
	//binary.BigEndian.PutUint64(idBytes, msg.Id)
	build.Write(idBytes)

	// need reply:0/1
	build.Write(bool2bytes(msg.NeedReply))
	// need ack:0/1
	build.Write(bool2bytes(msg.NeedAck))

	// length of session name(2 bytes)
	sessionNameLengthByte := []byte{0, 0}
	sessionNameBytes := []byte(msg.SessionName)
	lengthOfSessionName := len(sessionNameBytes)
	sessionNameLengthByte[1] = byte(lengthOfSessionName)
	sessionNameLengthByte[0] = byte(lengthOfSessionName >> 8)

	build.Write(sessionNameLengthByte)
	// session name
	build.Write(sessionNameBytes)

	// topicAttribute
	build.Write(map2String(msg.TopicAttribute))

	// appProps
	build.Write(map2String(msg.AppProps))

	// payload
	lengthPayloadBytes := []byte{0, 0, 0, 0}
	payloadBytes := []byte(msg.Body)
	lengthOfPayload := len(payloadBytes)
	lengthPayloadBytes[3] = byte(lengthOfPayload)
	lengthPayloadBytes[2] = byte(lengthOfPayload >> 8)
	lengthPayloadBytes[1] = byte(lengthOfPayload >> 16)
	lengthPayloadBytes[0] = byte(lengthOfPayload >> 24)
	build.Write(lengthPayloadBytes)
	build.Write(payloadBytes)
	//log.Errorf("ProtoMsg2String:%s, msg:%v", build.String(), msg)

	return base64.StdEncoding.EncodeToString(build.Bytes())
}

func String2protoMsg(inputStr string) (proto.Message, error) {
	msg := &proto.Message{}
	inputBytes, _ := base64.StdEncoding.DecodeString(inputStr)

	if len(inputBytes) < 10 {
		err := errors.Errorf("inputstr invalid:[%s], bytes[%d]", string(inputBytes), inputBytes)
		//err := fmt.Errorf("inputstr invalid:[%s], bytes[%d]", string(inputBytes), inputBytes)
		//log.Errorf("String2protoMsg failed,:[%v]", err)
		return *msg, err
	}

	var pos uint32
	if errFlag := []byte(inputBytes[0:1])[0]; byteOne == errFlag {
		errLength := binary.BigEndian.Uint32([]byte(inputBytes[1:5]))
		errorMessage := inputBytes[5 : 5+errLength]

		return *msg, fmt.Errorf(string(errorMessage))
	} else {
		pos = 5
		msgIdBytes := inputBytes[pos : pos+8]
		msg.Id = binary.BigEndian.Uint64(msgIdBytes)
		pos = pos + 8

		// need reply
		msg.NeedReply = byte2bool(inputBytes[pos])
		pos++

		// need ack
		msg.NeedAck = byte2bool(inputBytes[pos])
		pos++

		// length of session name
		lengthOfSessionName := uint32(binary.BigEndian.Uint16(inputBytes[pos : pos+2]))
		pos = pos + 2

		// session name
		msg.SessionName = string(inputBytes[pos : pos+lengthOfSessionName])
		pos = pos + lengthOfSessionName

		// topicAttribute
		totalTopicAttributeLength := binary.BigEndian.Uint32(inputBytes[pos : pos+4])
		pos = pos + 4

		if totalTopicAttributeLength > 0 {
			msg.TopicAttribute = string2Map(inputBytes[pos:pos+totalTopicAttributeLength], totalTopicAttributeLength)
			pos = pos + totalTopicAttributeLength
		} else {
			msg.TopicAttribute = make(map[string]string)
		}

		// appProps
		appPropsLength := binary.BigEndian.Uint32(inputBytes[pos : pos+4])
		pos = pos + 4

		if appPropsLength > 0 {
			msg.AppProps = string2Map(inputBytes[pos:pos+appPropsLength], appPropsLength)
			pos = pos + appPropsLength
		} else {
			msg.AppProps = make(map[string]string)
		}

		// payload
		msg.Body = string(inputBytes[pos+4:])

		return *msg, nil
	}
}
