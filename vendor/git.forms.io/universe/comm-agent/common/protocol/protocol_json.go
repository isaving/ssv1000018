//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package common

// Define all topic-related constants and platform-related parameter keys
const (
	TOPIC_TYPE = "type"

	TOPIC_SOURCE_ORG         = "srcOrg"
	TOPIC_SOURCE_AZ          = "srcAz"
	TOPIC_SOURCE_DCN         = "srcDcn"
	TOPIC_SOURCE_SERVICE_ID  = "srcSvrId"
	TOPIC_SOURCE_NODE_ID     = "srcNodeId"
	TOPIC_SOURCE_INSTANCE_ID = "srcInsId"

	TOPIC_DESTINATION_ORG         = "dstOrg"
	TOPIC_DESTINATION_DCN         = "dstDcn"
	TOPIC_DESTINATION_NODE_ID     = "dstNodeId"
	TOPIC_DESTINATION_INSTANCE_ID = "dstInsId"
	TOPIC_ID                      = "id"
	TOPIC_SHARE_NAME              = "shareName"

	RR_REPLY_TO           = "_REPLY_TO"
	TXN_IS_SEMI_SYNC_CALL = "_IS_SEMI_SYNC_CALL"
	IS_PAYLOAD_CRYPTO     = "_IS_PAYLOAD_CRYPTO"
	CRYPTO_ALGO           = "_CRYPTO_ALGO"
	CRYPTO_PADDING        = "_CRYPTO_PADDING"
	CRYPTO_MODE           = "_CRYPTO_MODE"
	CRYPTO_KEY_VERSION    = "_CRYPTO_KEY_VERSION"
	FIXED_KEY_ID          = "_FIXED_KEY_ID"
	DELIVERY_MODE         = "_DELIVERY_MODE"
	DST_TOPIC_ID          = "_DST_TOPIC_ID"
	DISCARD_RESPONSE      = "_DISCARD_RESPONSE"
	CS_START_TIMESTAMP    = "_CS_START_TIMESTAMP"
	SR_START_TIMESTAMP    = "_SR_START_TIMESTAMP"

	TARGET_DCN       = "_TARGET_DCN"
	DLS_ELEMENT_TYPE = "_DLS_ELEMENT_TYPE"
	DLS_ELEMENT_ID   = "_DLS_ELEMENT_ID"

	//topic_type
	TOPIC_TYPE_HEARTBEAT      = "HBT"
	TOPIC_TYPE_ERROR          = "ERR"
	TOPIC_TYPE_ALERT          = "ALT"
	TOPIC_TYPE_TRANSACTION    = "TRN"
	TOPIC_TYPE_LOG            = "LOG"
	TOPIC_TYPE_METRICS        = "MTR"
	TOPIC_TYPE_DTS            = "DTS"
	TOPIC_TYPE_UNIVERSE       = "OPS"
	TOPIC_TYPE_P2P            = "P2P"
	TOPIC_TYPE_SESSION_IN_BOX = "#P2P"
)

// Message is used to store the data structure of data conversion between go/c inside comm-agent server.
type Message struct {
	// Each interaction has a session unique identifier
	Id uint64

	// Used to save topic-related key-values pairs
	TopicAttribute map[string]string

	// True means this is a synchronous call message
	// False means this is an asynchronous call message
	NeedReply bool

	// If it's a synchronous call message, this field value is true
	// If it's a asynchronous call message, this field value is false
	NeedAck bool

	// SessionName is used to distinguish which session the message was sent to solace
	SessionName string

	// 0: means the delivery mode is direct
	// 1: means the delivery mode is persistent
	DeliveryMode int

	// app properties
	// service can set this attributes pass to target service
	AppProps map[string]string

	// message payload
	Body string
}

// SendQueueAckReq structure for ack queue request message
type SendQueueAckReq struct {
	MsgId uint64
}

// SendTopicMsgReq structure for publish messagess
type SendTopicMsgReq struct {
	Msg Message
}

// SendReplyMsgReq structure for sending reply messages
type SendReplyMsgReq struct {
	Msg      Message
	AckMsgId uint64
}

// SendRequestReplyReq structure for R/R (request/reply) messages
type SendRequestReplyReq struct {
	Msg Message
}

// CommonResponse, define the common message struct between comm-agent sdk and comm-agent server
type CommonResponse struct {
	// ErrorCode, -1 indicat error ,0 indicat success
	ErrorCode int
	// ErrorMsg, Used to store the error message
	ErrorMsg string
	// Data, Used to store the message body
	Data interface{}
}
