//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package controllers

import (
	jsoniter "github.com/json-iterator/go"

	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/common/event_handler/base"
	constant "git.forms.io/universe/dts/common/const"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/event/message"
	"github.com/astaxie/beego"
)

type DTSBaseController struct {
	DTSClientBaseHandler
	beego.Controller
}

// @Desc the Prepare() of DTSBaseController, which finds out DTS information from Request
func (b *DTSBaseController) Prepare() {
	rootXid := b.Ctx.Request.Header.Get(constant.TXN_PROPAGATE_ROOT_XID_KEY)
	parentXid := b.Ctx.Request.Header.Get(constant.TXN_PROPAGATE_PARENT_XID_KEY)
	dtsAgentAddress := b.Ctx.Request.Header.Get(constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS)

	traceId := b.Ctx.Request.Header.Get(constant.TRACE_ID_KEY)
	spanId := b.Ctx.Request.Header.Get(constant.SPAN_ID_KEY)
	parentSpanId := b.Ctx.Request.Header.Get(constant.PARENT_SPAN_ID_KEY)

	spanCtx := &compensable.SpanContext{
		TraceId:      traceId,
		SpanId:       spanId,
		ParentSpanId: parentSpanId,
	}

	txnCtx := compensable.CmpTxnCtsInstance.NewTxnCtx()

	txnCtx.SpanCtx = spanCtx
	//if "" != rootXid && "" != parentXid && "" != dtsAgentAddress {
	//	txnCtx := compensable.CmpTxnCtsInstance.NewTxnCtx()
	txnCtx.RootXid = rootXid
	txnCtx.ParentXid = parentXid
	txnCtx.DtsAgentAddress = dtsAgentAddress
	b.DTSCtx = txnCtx
	//}
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	txnMessage := &message.TxnMessage{}
	if err := json.Unmarshal(b.Ctx.Input.RequestBody, txnMessage); nil == err {
		b.Req = &client.UserMessage{
			Id:             txnMessage.Id,
			AppProps:       txnMessage.AppProps,
			TopicAttribute: txnMessage.TopicAttribute,
			NeedReply:      txnMessage.NeedReply,
			NeedAck:        txnMessage.NeedReply,

			//message payload
			Body: []byte(txnMessage.Body),
		}
	}
}

// @Desc the Finish() of DTSBaseController, which describes response according to transaction's result
func (b *DTSBaseController) Finish() {
	txnMsg := &message.TxnMessage{}
	if nil != b.Rsp {
		txnMsg.Id = b.Rsp.Id
		txnMsg.TopicAttribute = b.Rsp.TopicAttribute
		txnMsg.NeedReply = b.Rsp.NeedReply
		txnMsg.NeedAck = b.Rsp.NeedAck
		txnMsg.AppProps = b.Rsp.AppProps

	}
	if nil == b.Rsp && b.Err != nil {
		errRes := base.CommonResponse{
			ErrorCode: -1,
			ErrorMsg:  b.Err.Error(),
		}
		json := jsoniter.ConfigCompatibleWithStandardLibrary
		errResJson, _ := json.Marshal(errRes)
		txnMsg.Body = string(errResJson)
		b.Data[constant.MESSAGE_TYPE_JSON] = *txnMsg
	} else {
		txnMsg.Body = string(b.Rsp.Body)
	}
	b.Data[constant.MESSAGE_TYPE_JSON] = *txnMsg
	b.ServeJSON()
	// clean DTSCtx
	b.DTSCtx = nil

}
