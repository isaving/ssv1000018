//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/sv/ssv1000018/models"
	"git.forms.io/isaving/sv/ssv1000018/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000018Controller struct {
	controllers.CommTCCController
}

func (*Ssv1000018Controller) ControllerName() string {
	return "Ssv1000018Controller"
}

// @Desc ssv1000018 controller
// @Description Entry
// @Param ssv1000018 body models.SSV1000018I true "body for user content"
// @Success 200 {object} models.SSV1000018O
// @router /ssv1000018 [post]
// @Author
// @Date 2020-12-12
func (c *Ssv1000018Controller) Ssv1000018() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000018Controller.Ssv1000018 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000018I := &models.SSV1000018I{}
	if err := models.UnPackRequest(c.Req.Body, ssv1000018I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv1000018I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000018 := &services.Ssv1000018Impl{}
	ssv1000018.New(c.CommTCCController)
	ssv1000018.Ssv100018I = ssv1000018I
	ssv1000018Compensable := services.Ssv1000018Compensable
	//ssv1000018O, err := ssv1000018.Ssv1000018(ssv1000018I)

	proxy, err := aspect.NewDTSProxy(ssv1000018, ssv1000018Compensable, c.DTSCtx)
	if err != nil {
		log.Errorf("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv1000018I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv0000006O, ok := rsp.(*models.SSV1000018O); ok {
		if responseBody, err := models.PackResponse(ssv0000006O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv1000018 Controller
// @Description ssv1000018 controller
// @Param Ssv1000018 body models.SSV1000018I true body for SSV1000018 content
// @Success 200 {object} models.SSV1000018O
// @router /create [post]
/*func (c *Ssv1000018Controller) SWSsv1000018() {
	//Here is to generate API documentation, no need to implement methods
}
*/
