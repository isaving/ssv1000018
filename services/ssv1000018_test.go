package services

import (
	"errors"
	"fmt"
	"git.forms.io/isaving/sv/ssv1000018/models"
	jsoniter "github.com/json-iterator/go"
	. "reflect"
	"testing"
)

//1. 可以根据条件返回不同的报文
//2. 可以传入多个请求报文 来进行不同的分支覆盖 (主要解决if-else过多的情况,这个是代码优化点, 请尽量不用if-else嵌套)
//3. 可以排列组合触发不同下游的err 用以覆盖不同的判错分支
//4. 能够覆盖解包错误  调下游打包错误可以删掉 这段代码是不会报错的
//5. 能够传入报文头
//6. 能够打印返回的内容
//=========================改下面的=====================================

//这个要改成service的结构
func (this *Ssv1000018Impl) RequestSyncServiceWithDCN(dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

//这个要改成service的结构
func (this *Ssv1000018Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

func (this *Ssv1000018Impl) RequestServiceWithDCN(dstDcn, serviceKey string, requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
	responseData = []byte(`{"Code":0,"Data":{"BS-LEGO":["C03101"]},"Message":"","errorCode":0,"errorMsg":"","response":{"BS-LEGO":["C03101"]}}`)
	return
}

func TestService(t *testing.T) {
	runTest(Ssv1000018Impl{}, "TrySsv1000018") //这两个都要改哦
	runTest(Ssv1000018Impl{}, "ConfirmSsv1000018")
	runTest(Ssv1000018Impl{}, "CancelSsv1000018")
}

//报文头
var SrcAppProps = map[string]string{
	"TxUserId":   "1001",
	"TxDeptCode": "1001",
}

//输入请求
var request = []models.SSV1000018I{
	{
		Account: "1001",
		Amount:  100.00,
		TrnDate: "1",
	},
	{
		Account: "1002",
		Amount:  20.00,
		TrnDate: "2",
	},
}

/* []models.IL120010I{
	{
		LoanDubilNo: "123312313123",
		NewMatrDt:   "20200820",
	},
	{
		LoanDubilNo: "123123123123",
		NewMatrDt:   "20200820",
	},
}*/

//使用反引号可以直接换行 还不会被带双引号影响
var response = map[string]interface{}{
	//这个key 是你调用RequestSyncServiceElementKey()这个函数传的serviceKey的字符串 不一定是topic 好好看看自己的代码传的字符串是啥
	//可以直接粘贴报文 也可以写一个返回结构体
	"ac000005": `{"returnCode": "0","returnMsg": "success","data": {"AccStatus":"1","AmtAvailable":5,"AmtCurrent":20,"AmtFreezing":50,"AmtLast":10}}`,

	/*"DAACRTC3": models.DAACRTC3O{
		LogTotCount: "1",
		Records: []models.DAACRTC3ORecords{{
			AccmCmpdAmt:    0,
			AccmIntSetlAmt: 0,
			AccmWdAmt:      0,
			AcctgAcctNo:    "",
			AcruUnstlIntr:  0,
			RecordNo:       0,
			RepaidInt:      0,
			RepaidIntAmt:   0,
			Status:         "",
			UnpaidInt:      0,
			UnpaidIntAmt:   0,
		}},
	},
	"DAILUDK1": `{"Form":[{"FormData":{"State":"ok"}}]}`,
	"DAILRDK3": models.DAILRDK3O{
		Records: []models.DAILRDK3ORecords{{
			LoanDubilNo:           "adfasdfasdf",
			RecalDt:               "",
			SeqNo:                 0,
			CustNo:                "",
			RemainPrin:            0,
			ActlstPrin:            0,
			ActlstIntr:            0,
			Pridnum:               0,
			KeprcdStusCd:          "",
			CurrPeriodRecalFlg:    "",
			CurrPeriodIntacrBgnDt: "",
			CurrPeriodIntacrEndDt: "",
			RepayDt:               "",
			PlanRepayTotlAmt:      0,
			PlanRepayPrin:         0,
			PlanRepayIntr:         0,
		}},
		PageNo:       1,
		PageRecCount: 10,
	},
	"DAACRAN1": models.DAACRAN1O{
		Balance: 0,
	},

	"IL1QS088": models.IL1QS088O{
		Records: []models.IL1QS088ORecords{{
			Pridnum: 0,
		}},
		TotCount: 1,
	},

	"DAILUDK3": `{"Form":[{"FormData":{"State":"ok"}}]}`,
	"DAILCDK3": `{"Form":[{"FormData":{"State":"ok"}}]}`,

	"DAILRFK1": models.DAILRFK1O{Records: []models.DAILRFK1ORecords{{
		MakelnAplySn: "",
	}}},

	"DAILUFK1": `{"Form":[{"FormData":{"State":"ok"}}]}`,*/
}

//func (this *Ssv1000018Impl)RequestServiceWithDCN(dstDcn, serviceKey string,requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
//	if serviceKey == "DlsuDcnLists" {
//
//	}
//}

//==========================就用改上面的就行了=======================================
//==========================就用改上面的就行了=======================================
//==========================就用改上面的就行了=======================================
//==========================就用改上面的就行了=======================================

var errServiceKey []string

//初始化
func init() {
	for serviceKey := range response {
		errServiceKey = append(errServiceKey, serviceKey)
	}
}

var currentIndex = 0
var passed bool

//排列组合返回报文
func RequestService(serviceKey string, reqBytes []byte) (responseData []byte, err error) {

	if serviceKey == errServiceKey[currentIndex] && passed {
		currentIndex++
		if currentIndex >= len(errServiceKey) {
			currentIndex = 0
		}
		return nil, errors.New("error")
	}

	//这里进行条件控制

	switch response[serviceKey].(type) {
	case string:
		responseData = []byte(response[serviceKey].(string))
	default:
		responseData = getByte(response[serviceKey])
	}
	return
}

func getByte(v interface{}) (bt []byte) {
	bt, _ = jsoniter.Marshal(struct {
		Form [1]struct{ FormData interface{} }
	}{Form: [1]struct{ FormData interface{} }{{v}}})
	return
}

func runTest(serviceStruct interface{}, method string) {
	param := make([]Value, 1)

	for _, req := range request {

		//这里先执行一次覆盖validate
		param[0] = New(TypeOf(req))
		callMethod(serviceStruct, method, param)

		param[0] = ValueOf(&req)
		passed = false
		for i := 0; i < len(response); i++ {
			callMethod(serviceStruct, method, param)
			passed = true
		}
	}

}

func callMethod(serviceStruct interface{}, method string, params []Value) {
	service := New(TypeOf(serviceStruct))
	service.Elem().FieldByName("SrcAppProps").Set(ValueOf(SrcAppProps))
	ret := service.MethodByName(method).Call(params)
	fmt.Printf(" 返回 [ %v ] \n 错误 [ %v ]\n\n", ret[0], ret[1])
}

type ServiceKey string
type Responders struct {
}

func when(servicekey ServiceKey) *Responders {

	return nil
}

func (this *Responders) If(field string, value interface{}) {

}

func (this *Responders) setResponse(response interface{}) {

}

//doReturn("IL8R0022").when("status","ok").thenReturn()
//responseFrom("query_system_const").when("Status","ok").thenReturn()
//requestTo("query_system_const").when("xx","1").thenReturn(xx)

//when("DAILRDK3").If("RepayDate","2020-01-02").setResponse()
//when("DAILRDK3").setAnotherResponse()
