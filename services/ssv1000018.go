//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/constant"
	"git.forms.io/isaving/sv/ssv1000018/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)

var Ssv1000018Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv1000018",
	ConfirmMethod: "ConfirmSsv1000018",
	CancelMethod:  "CancelSsv1000018",
}

type Ssv1000018 interface {
	TrySsv1000018(i *models.SSV1000018I) (*models.SSV1000018O, error)
	ConfirmSsv1000018(*models.SSV1000018I) (*models.SSV1000018O, error)
	CancelSsv1000018(*models.SSV1000018I) (*models.SSV1000018O, error)
}

type Ssv1000018Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Ssv100018O *models.SSV1000018O
	Ssv100018I *models.SSV1000018I
}

// @Desc Ssv1000018 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000018Impl) TrySsv1000018(ssv1000018I *models.SSV1000018I) (ssv1000018O *models.SSV1000018O, err error) {

	impl.Ssv100018I = ssv1000018I
	impl.Ssv100018O = &models.SSV1000018O{}

	//TODO Service Business Process
	log.Debug("Call AC000005 start！")
	if err := impl.CallAC000005(); err != nil {
		return nil, err
	}

	ssv1000018O = &models.SSV1000018O{
		//TODO Assign  value to the OUTPUT Struct field
		AmtFreezing:  impl.Ssv100018O.AmtFreezing,
		AmtCurrent:   impl.Ssv100018O.AmtCurrent,
		AmtLast:      impl.Ssv100018O.AmtLast,
		AmtAvailable: impl.Ssv100018O.AmtAvailable,
		AccStatus:    impl.Ssv100018O.AccStatus,
	}

	return ssv1000018O, nil
}
func (impl *Ssv1000018Impl) ConfirmSsv1000018(ssv1000018I *models.SSV1000018I) (ssv0000006O *models.SSV1000018O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv0000006")
	return nil, nil
}

func (impl *Ssv1000018Impl) CancelSsv1000018(ssv1000018I *models.SSV1000018I) (ssv0000006O *models.SSV1000018O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv0000006")
	return nil, nil
}

func (impl *Ssv1000018Impl) CallAC000005() error {
	impl.Ssv100018O = &models.SSV1000018O{}
	SSV1000018I := models.SSV1000018I{
		Account: impl.Ssv100018I.Account,
		Amount:  impl.Ssv100018I.Amount,
		TrnDate: impl.Ssv100018I.TrnDate,
	}
	log.Debug("SSV1000018I=:", SSV1000018I)
	reqBody, err := models.PackRequest(SSV1000018I)
	if nil != err {
		return errors.New("", constant.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constant.AC000005, reqBody)
	if err != nil {
		log.Error("Call AC000005 filed")
		return err
	}

	QuerySSV1000018O := models.SSV1000018O{}
	err = QuerySSV1000018O.UnPackResponse(resBody)
	if err != nil {
		return errors.New("Unpacking error", constant.ERRCODE2)
	}
	log.Debug("QuerySSV9000002O:", QuerySSV1000018O)
	impl.Ssv100018O = &QuerySSV1000018O
	return nil
}
