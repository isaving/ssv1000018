//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000018I struct {
	Account string  `validate:"required" description:"Accounting account"` //核算账号        Y
	Amount  float64 `validate:"required" description:"Frozen amount"`      //冻结金额        Y
	TrnDate string  `validate:"required" description:"Business date"`      //业务日期        Y
}

type SSV1000018O struct {
	AmtFreezing  float64 `description:"Frozen amount"`           //冻结金额
	AmtCurrent   float64 `description:"Current balance"`         //当前余额
	AmtLast      float64 `description:"Previous period balance"` //上期余额
	AmtAvailable float64 `description:"Available Balance"`       //可用余额			可用余额=当前余额-冻结金额-预留金额
	AccStatus    string  `description:"Account Status"`          //账户状态
}

// @Desc Build request message
func (o *SSV1000018I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000018I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000018O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000018O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000018I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000018I) GetServiceKey() string {
	return "ssv1000018"
}
